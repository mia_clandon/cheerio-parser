import cheerio from "cheerio";
import got from "got";

import {IGetScrapedUrlData} from './parse.d';

export function scrapeUrlData(url: string): Promise<IGetScrapedUrlData> {
    return new Promise((resolve, reject) => {
        let sdata: IGetScrapedUrlData = {} as IGetScrapedUrlData;

        got(url)
            .then((response) => {

                const $ = cheerio.load(response.body);

                sdata.articles = [];

                // let idList = [];
                // let allAs = $("a").filter('data-page');
                // for(var i = 0; i < allAs.length; i++) {
                //     idList.push(allAs[i].attr['data-page']);
                //     console.log(idList);
                //
                //     $('article').each((i, item) => {
                //         const time = $(item)
                //             .find('div.right')
                //             .find('time')
                //             .text()
                //         const category = $(item)
                //             .find('div.right')
                //             .find('p.time')
                //             .text()
                //         const title = $(item)
                //             .find('div.right')
                //             .find('h2')
                //             .find('a')
                //             .attr('title')
                //         const description = $(item)
                //             .find('div.right')
                //             .find('div.text')
                //             .find('p')
                //             .text()
                //         const images = $(item)
                //             .find('figure')
                //             .find('a')
                //             .find('img')
                //             .attr('src')
                //
                //         // console.log(title, description, images)
                //         sdata.articles.push({
                //             category, time, title, description, images
                //         });
                // }

                $('article').each((i, item) => {
                    const time = $(item)
                        .find('div.right')
                        .find('time')
                        .text()
                    const category = $(item)
                        .find('div.right')
                        .find('p.time')
                        .text()
                    const title = $(item)
                        .find('div.right')
                        .find('h2')
                        .find('a')
                        .attr('title')
                    const description = $(item)
                        .find('div.right')
                        .find('div.text')
                        .find('p')
                        .text()
                    const images = $(item)
                        .find('figure')
                        .find('a')
                        .find('img')
                        .attr('src')

                    // console.log(title, description, images)
                    sdata.articles.push({
                        category, time, title, description, images
                    });

                    // console.log(sdata.articles)

                    return resolve(sdata);

                })
            })
            .catch((err) => reject(err));

    })
}