export interface IGetScrapedUrlData {
    [articles: string]: { category:string, time: string, title: string, description: string, images: string }[];
}
