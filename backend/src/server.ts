import express from 'express';
import morgan from 'morgan';
import { runModeConfig } from './utils/args';
import routes from './routes/index';
const app = express();

app.use(morgan('dev'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", runModeConfig.accessControlAllowOrigin);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.use(express.json());

app.use(routes);

// @ts-ignore
app.listen(runModeConfig.servePort, (err: Error) => {
  if (err) {
    return console.error(err);
  }
  return console.log('Success! Serving on port  4002');
});
