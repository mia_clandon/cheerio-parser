import express from 'express';

import { IGetScrapedUrlData } from '../parser/parse.d';

import { scrapeUrlData } from '../parser/parse';

const router = express.Router();


router.get('/url-scrape', (req, res, next) => {
    const { url } = req.query;

    // @ts-ignore
    scrapeUrlData(url)
        .then((sdata: IGetScrapedUrlData) => {
            return res.json({ data: sdata, err: "" });
        })
        .catch((err) => {
            return res.json({ data: null, err: err.message });
        });
});


export default router;
