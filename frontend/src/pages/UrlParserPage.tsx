import * as React from 'react';

import UrlParserInfo from '../parts/UrlParserInfo/UrlParserInfo';


interface IUrlParserPagerops {}
interface IUrlParserPageState {}

export default class UrlParserPage extends React.Component<IUrlParserPagerops, IUrlParserPageState> {
    public render() {
        return (
            <div className="url-scraper-page">
                <UrlParserInfo />
            </div>
        );
    }
}
