interface IApiConfig {
    base: IApiBaseUrlConfig
}

interface IApiBaseUrlConfig {
    dev: string;
    prod: string;
}


const config: IApiConfig = {
    base: {
        dev: "http://localhost:4002",
        prod: ""
    }
}

export default config;
