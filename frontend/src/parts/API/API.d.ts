import { AxiosResponse } from 'axios';

export interface IDefaultJsonReturn<T> {
    err: string;
    data: T;
}

export interface IGetScrapedUrlData {
    [articles: string]:
        { category: string, time: string, title: string, description: string, images: string }[];
}

export interface IGetScrapedUrlDataResponse extends AxiosResponse<IDefaultJsonReturn<IGetScrapedUrlData>> {}
