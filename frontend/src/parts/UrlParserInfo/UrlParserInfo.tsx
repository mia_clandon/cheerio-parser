import * as React from 'react';
import {getScrapedUrlData} from '../API/API';
import {IGetScrapedUrlData} from '../API/API.d';

interface IUrlParserInfoProps {
}
interface IUrlParserInfoState {
    scrapeData: IGetScrapedUrlData | null,
    dataFetching: boolean;
}
export default class UrlParserInfo extends React.Component<IUrlParserInfoProps, IUrlParserInfoState> {
    constructor(props: IUrlParserInfoProps) {
        super(props);

        this.state = {
            scrapeData: null, dataFetching: false,
        }
    }

    public render() {
        const newUrl: string = "https://bolapresa.com.br/";
        getScrapedUrlData(newUrl)
            .then(({data: resJson}) => {
                const {data, err} = resJson;
                if (err !== "") {
                    this.setState({
                        scrapeData: null, dataFetching: false
                    });
                } else {
                    this.setState({
                        scrapeData: data, dataFetching: false
                    })
                }
            })
            .catch((err) => {
                this.setState({
                    scrapeData: null, dataFetching: false
                })
            });
        const sdata: IGetScrapedUrlData | null = this.state.scrapeData;
        const scrapeDataVisStyle = sdata && !this.state.dataFetching ? {} : {display: 'none'};

        return (<div>
            <div style={scrapeDataVisStyle}>
                {sdata ? sdata.articles.map(article => (<article>
                    <div className="left">
                        <img src={article.images} alt="" width="550px" height="370px"/>
                    </div>
                    <div className="right">
                        <div className="top_header">
                            <time>{article.time}</time>
                            <p>{article.category}</p>
                        </div>
                        <h2>{article.title}</h2>
                        <div>
                            <p>{article.description}</p>
                        </div>
                    </div>
                </article>)) : ""}
            </div>
        </div>);
    }
}


