import * as React from 'react';
import './style/global.css';
import './App.css';
import UrlParserPage from "./pages/UrlParserPage";


interface IAppProps {}
interface IAppState {}


class App extends React.Component<IAppProps, IAppState> {
  public render() {
    return (
      <div className="App">
            <UrlParserPage />
      </div>
    );
  }
}

export default App;
